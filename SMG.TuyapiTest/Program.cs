﻿
using SMG.TuyaApi.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace SMG.TuyapiTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var time = new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds();
            var timeexp = new DateTimeOffset(DateTime.Now.AddMinutes(30)).ToUnixTimeMilliseconds();
            string deviceId = "bfc759d162265a9920zd88";

            var q = new TuyaApi.TuyaApi();
            q.url = Regions.URL_EU;
            q.client_id = "your_client_id";
            q.secret = "your_client_secret";

 
            //q.getToken();
            //q.refreshToken();

 

            var info = q.getDeviceInformation(deviceId);
            var deviceName = info.result.name;
            var isOnline = info.result.online;
            var uptime = info.result.update_time;
            var createtime = info.result.create_time;
            var activetime = info.result.active_time;
            var uid = info.result.uid;
            var users = q.getUserInfo(uid);

            var devlist1 = q.getDeviceList(uid);

            var dfn = q.getDeviceFunctions(deviceId);

            var ds = q.getDeviceStatus(deviceId);
            //volume control example
            //q.sendCommand(deviceId, "volume_control", 5);
            //var fl = q.getFunctionList();
            //var cmd = q.sendCommand(deviceId, "switch", false);
            List<CommandModel.Command> commands = new List<CommandModel.Command>();
            commands.Add(new CommandModel.Command { code = "switch", value = true });
            commands.Add(new CommandModel.Command { code = "mode", value = "heat" });
            commands.Add(new CommandModel.Command { code = "fan", value = "high" });
            commands.Add(new CommandModel.Command { code = "temp", value = 24 });
           var result= q.sendCommands(deviceId, commands);
            if (result.success)
            {
                Console.WriteLine("send successfully!");
            }
            else
            {
                Console.WriteLine($"execute problem, reason:{result.msg}");
            }

            ClimateExample("bfc759d162265a9920zd88", q);

            q.TurnSwitchOn("47000505483fda79531a");
            Thread.Sleep(2000);
            q.TurnSwitchOff("47000505483fda79531a");
        }

        private static void ClimateExample(string deviceId, TuyaApi.TuyaApi q)
        {
            q.TurnOn(deviceId);
            Thread.Sleep(1000);
            q.setClimateMode(deviceId, ClimateCommands.enModes.cold);
            Thread.Sleep(1000);
            q.setClimateTemperature(deviceId, 22);
            Thread.Sleep(1000);
            q.setClimateFan(deviceId, ClimateCommands.enFanModes.high);
            Thread.Sleep(5000);
            q.TurnOff(deviceId);
        }
    }
}

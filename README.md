# README #

This Api provides basic access to TUYA api . 

why i need that? 

well.. i have switchs, gang switches, infrared hubs.. :) 

there is no c# api which can handle  infrared hubs or devices created in infrared hubs.. 

so i wrote mine.. 
(this api can control any Tuya Enabled device.. not only infrared hubs.. bulbs, switches, gang switches, plugs etc)

forexample, these devices are infrared controlled devices.. actually they are virtual devices.. but Tuya system added them as seperate devices.. so each device seems "real" device :)) 

 i can control yamaha av receiver, bosch air conditioner, LG WebOS, technicolor satellite receiver etc with that device.. 

![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/6fb780fcce1a3436443d7f965694bb6bec1abcdf/img/infraredDevices.png)


which infrared hub?

https://www.aliexpress.com/item/4000203065257.html

![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/f3c255e82847ab947fb371ac07972ea3822023e7/img/infraredHub.png)

why? because it was very cheap :)  10 bucks.. 

20.000 devices already in their app.. just add new device, select type, select brand.. test.. bam.. works.. 

it does not need to directly see the devices.. just put anywhere in same room.. 

### How to Start? ###

a basic setup 

```
var q = new TuyaApi.TuyaApi();
//you must select which region you will use
q.url = Regions.URL_EU;
//client_id from iot.tuya.com->cloud->projects->your project
// generate authorization key and secret from there and use in here
q.client_id = "your_client_id";
q.secret = "your_client_secret";

//example deviceId
string deviceId = "bfc759d162265a9920zd88";
DeviceInformationModel.Device info = q.getDeviceInformation(deviceId);
var deviceName = info.result.name;
var isOnline = info.result.online;
var uptime = info.result.update_time;
var createtime = info.result.create_time;
var activetime = info.result.active_time;
var uid = info.result.uid;
```



**What Features supported?**

```
TokenModel.Token getToken();
DeviceInformationModel.Device getDeviceInformation(string deviceId);
DeviceStatusModel.DeviceStatus getDeviceStatus(string deviceId);
DeviceListModel.DeviceList getDeviceList(string uid);
UserInfoModel.UserInfo getUserInfo(string uid);
DeviceFunctionsModel.DeviceFunctions getDeviceFunctions(string deviceId);
BaseModel sendCommand(string deviceId, string command, object value);
BaseModel sendCommands(string deviceId, List<CommandModel.Command> commands);
BaseModel TurnSwitchOn(string deviceId);
BaseModel TurnSwitchOff(string deviceId);
BaseModel TurnOn(string deviceId);
BaseModel TurnOff(string deviceId);
BaseModel setClimateMode(string deviceId, ClimateCommands.enModes mode);
BaseModel setClimateMode(string deviceId, string mode);
BaseModel setClimateTemperature(string deviceId, int Temp);
BaseModel setClimateFan(string deviceId, ClimateCommands.enFanModes mode);
BaseModel setClimateFan(string deviceId, string mode);
```



### Lets deep dive, shall we? ###

* **Where to start?**

  * install Smart Life or Tuya App to your phone (i used Smart Life)

  * register with email and password, **"not"** with google authentication etc.. just email and password. it might be a gmail mail, but "**not authorize**" with gmail etc.. (i said i used Smart Life :) because i first installed Tuya app and used google authentication and could not get api features, so i switched to Smart Life, it seems they are all same but uses different usernames and passwords to get in.. also iot.tuya.com uses different username and password.. so if you register with Smart Life, it will not be used in iot.tuya.com, so you must register **again** with same email or other.. )

  * navigate https://iot.tuya.com/ and create an account.

  * follow the steps to add your tuya compatible devices from https://github.com/codetheweb/tuyapi/blob/master/docs/SETUP.md , usually , 1-3 steps enough for "Listing Tuya devices from the Tuya Smart or Smart Life apps" . register as many feature as you can.. 

  * you will get your client id and password from a screen like this (cloud->projects->create a project)

    ![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/f3c255e82847ab947fb371ac07972ea3822023e7/img/secret.png)

  * as in that picture, it also show linked devices (mine was 11 device at the time i created this document)

  * to access  your devices, cloud->projects->your create project->device list

    ![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/f3c255e82847ab947fb371ac07972ea3822023e7/img/devices.png)

    now you are ready to use the api.. 

  

### what else do we need now? ###

* well.. you need user uid :) but where is it?

  * what is user uid? it is a unique id to get some extra features from commands..

  * where is it? nowhere in iot.tuya.com but their api explorer shows examples with it.. but they forgot to put that value in a place on their website.. anyway, there is a solution..  

  * when you get token, it shows a **uid**, but.. it is not the user uid tuya openapi likes.. 

  * lets get uid for future needs,first write down one of deviceid to notepad then lets get user uid.:

```
    var q = new TuyaApi.TuyaApi();
    //you must select which region you will use
    q.url = Regions.URL_EU;
    //we get both at upper screens..
    q.client_id = "your_client_id";
    q.secret = "your_client_secret";
    
    //now lets use deviceId here
    string deviceId = "bfc759d162265a9920zd88";
    DeviceInformationModel.Device info = q.getDeviceInformation(deviceId);
    //this is your user uid.. it is unique and will not change anymore..
    var uid = info.result.uid;
    //lets check who am i..
     var userInfo = q.getUserInfo(uid);
    //with this uid, we can get all our devices now, which are already at iot.tuya.com
    var devlist = q.getDeviceList(uid);
```

output device list seems something like this:

![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/f3c255e82847ab947fb371ac07972ea3822023e7/img/devicelist.png)


  now we have all deviceIDs in our hands.. 

  each device have list of status.. which means we can use.. 

 ![image-20210330190747118](img/deviceStatus.png)

  it seems it is a switch and can be true/false.. well lets use this feature.. 

```
  var cmd = q.sendCommand("yourdeviceId", "switch_1", true);
```

  :) yeah, that switch now "on" state..  

  also result will tell us if the command executed successfully.. 

  i also implemented this command into a simpler method.. but you may build yours also

```
  q.sendCommand("yourdeviceId", "switch_1", true);
  //you can use TurnSwitchOn also, both do the same.. 
  q.TurnSwitchOn("yourdeviceId");
  //or
  q.TurnSwitchOff("yourdeviceId");
  //it does the same as sendCommand..  
```

  **what are other device functions?**

   lets get all supported functions of device:

```
  var info = q.getDeviceFunctions(deviceId);
```

  screenshot:

  ![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/f3c255e82847ab947fb371ac07972ea3822023e7/img/functions.png)

  

  output as json:

```json
  {
    "result": {
      "category": "infrared_ac",
      "functions": [
        {
          "code": "switch",
          "desc": "switch",
          "name": "switch",
          "type": "BOOLEAN",
          "values": true
        },
        {
          "code": "mode",
          "desc": "mode",
          "name": "mode",
          "type": "ENUM",
          "values": {
            "range": [
              "dehumidification",
              "cold",
              "auto",
              "wind_dry",
              "heat"
            ]
          }
        },
        {
          "code": "temp",
          "desc": "temp",
          "name": "temp",
          "type": "INTEGER",
          "values": {
            "max": 30,
            "min": 16,
            "scale": 0,
            "step": 1,
            "type": "Integer",
            "unit": "℃"
          }
        },
        {
          "code": "fan",
          "desc": "fan",
          "name": "fan",
          "type": "ENUM",
          "values": {
            "range": [
              "low",
              "mid",
              "high",
              "auto"
            ]
          }
        }
      ]
    },
    "success": true,
    "t": "2021-03-30T16:50:57.273Z",
    "code": 0,
    "msg": null
  }
```

  

  lets use getDeviceFunctions method with an example..

```
  //assume your deviceId is for a AirConditioner
  string deviceId="yourdeviceid";
  var dfn = q.getDeviceFunctions(deviceId);
  var isSuccess=dfn.success;
  var myfunctions=dfn.result.functions;
  //lets get "mode"
  var testFunction=myfunctions[1]; //other functions were mode,fan,temp etc..
  var code=testFunction.code; //shows "mode"
  var val=testFunction.values[0];   // can be dehumidification,cold,auto,wind_dry,heat
  
  //lets use
  //first lets power on device, (all functions were in myfunctions array.. check them..)
  q.sendCommand("yourdeviceId", "switch", true); //this device have "switch" feature, not "switch_1", some devices may have different code names :)
  //now lets set mode to heat 
  q.sendCommand("yourdeviceId", "mode", "heat");
  //then lets set fan mode :)
  q.sendCommand("yourdeviceId", "fan", "high"); //other features were low,mid,auto,high
  //then lets set temperature
  q.sendCommand("yourdeviceId", "temp", 24); //it seems its between 16-30
  //wait 5 seconds
  Thread.Sleep(5000);
  //power off
  q.sendCommand("yourdeviceId", "switch", false);
  
  //you may use predefined functions as well, does the same..
  //i am a lazy coder.. write once, use many.. 
  q.TurnOn(deviceId);
  Thread.Sleep(1000);
  q.setClimateMode(deviceId, ClimateCommands.enModes.cold);
  Thread.Sleep(1000);
  q.setClimateTemperature(deviceId, 22);
  Thread.Sleep(1000);
  q.setClimateFan(deviceId, ClimateCommands.enFanModes.high);
  Thread.Sleep(5000);
  q.TurnOff(deviceId);
```

  as you see, command values can be boolean, integer or string.. 

  **where are the device saved states?**

  if device sends its state to tuya servers.. you can get them also.. 

  but.. some infrared devices and some other devices are one-way device.. so they may not send states (status) to tuya server.. so when you get device status, you may get "not supported" or null as result..

  lets test:

```
  var ds = q.getDeviceStatus(deviceId);
  //lets get saved temperature
  var savedTemp=ds.result[2];
  var code=savedTemp.code; //it shows "temp"
  var value=savedTemp.value; //it shows 22 
```

  screenshot:

 ![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/f3c255e82847ab947fb371ac07972ea3822023e7/img/deviceState.png)


### what more? ###

there is one more sendCommand feature.. it is batch command sending... 

```
List<CommandModel.Command> commands = new List<CommandModel.Command>();
commands.Add(new CommandModel.Command { code = "switch", value = true });
commands.Add(new CommandModel.Command { code = "mode", value = "heat" });
commands.Add(new CommandModel.Command { code = "fan", value = "high" });
commands.Add(new CommandModel.Command { code = "temp", value = 24 });
var result= q.sendCommands(deviceId, commands);
if (result.success)
{
    Console.WriteLine("send successfully!");
}
else
{
    Console.WriteLine($"execute problem, reason:{result.msg}");
}
```



**result**

use , change,contribute, add new features, do whatever you want.. 

and send me if you add new feature :)) 

for questions: serkan@smg.com.tr



**what next?**

i will implement "**Magic Home**" device support.. why? :))) accidentally i bought 5-6  bulbs which are not compatible with tuya.. but they are compatible with Magic Home.. so i want to use them also.. 

still couldn't find documentation.. or the ones written in nodejs, python and c# are not working with my bulbs.. i tested all.. i think i will reverse engineer the data between Magic Home mobile app and servers.. i will setup a proxy server and grab all data.. bla bla bla.. i will write the results later.. 

**then what?**

i will implement all these features to Home Assistant https://www.home-assistant.io/.. its a home automation system.. i loved it.. i implemented my Yamaha RX-V775 (with direct access to receiver with rest services, not with irhub), LG Webos, Technicolor sattellite receivers, special cards for ecovacs deebot osmo 950 vacuum cleaner (their mobile app sucks.. not working with Turkish Language.. it does not show map.. but if you switch phone language to English, it shows.. damn stupid developers) already..  there is a Netdaemon addon https://netdaemon.xyz/.. which i can use c# code with HAS.. it seems perfect.. 

if i can't do it or hate.. i will switch to nodejs.. if can't or hate.. i will switch to python.. 

so you may ask, if you can write in python, why not python, hass uses python? well.. i hate python.. :)  i like c# more.. :))  then nodejs.. and c++ (already implemented some ESP32 based works with HAS).. but not python.. 



 ![alt_text](https://bitbucket.org/serkanp/smg.tuyaapi/raw/6fb780fcce1a3436443d7f965694bb6bec1abcdf/img/esp32.png)


**done!**

you may dismiss.. 
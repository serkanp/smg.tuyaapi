﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SMG.TuyaApi
{
    public static class helper
    {
        public static string CreateToken(this string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return ByteArrayToString(hashmessage).ToUpper();
            }
        }
        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }
        
        public static DateTimeOffset toDateTime(this int unixTime)
        {
            return DateTimeOffset.FromUnixTimeSeconds(unixTime);
        }
         
    }
}
